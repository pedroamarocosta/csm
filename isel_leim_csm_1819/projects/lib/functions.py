from IPython.core.display import display, HTML, Math, Latex

""" 
    Library class for displaying data in latex format 
        - The cells need to be set as markdown to avoid displaying latex as code prior to output.
        - References:
            https://physics.meta.stackexchange.com/questions/6461/is-there-a-way-to-control-the-font-size-of-mathjax-equations
            https://math.meta.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference
            
"""


class CustomLatex:
    @staticmethod
    def text(text: str) -> None:
        text = text.replace(' ', '\\ ')  # Spaces in Latex are represented by '/', and '/' is escaped by '//'
        display(Math(r'\small\mathrm{{{0}}}$'.format(text)))


